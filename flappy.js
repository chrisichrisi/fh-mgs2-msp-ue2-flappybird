﻿
var Time = {
    DT: 0,
    Last_Absolute: 0,
    Now_Absolute: 0,

    Update: function (timestamp) {
        this.Last_Absolute = this.Now_Absolute;
        this.Now_Absolute = timestamp;
        this.DT = (this.Now_Absolute - this.Last_Absolute) / 1000;
        if (this.DT > 1) { this.DT = 0.1; } // Fix for debugging
    }
}

var GameStates = {
    PreRunning: 0,
    Running: 1,
    Paused:  2,
    Ended:   3
}

var Keys = {
    ESC: 27,
    Space: 32,
    P: 80
}

var Game = {
    State: GameStates.PreRunning,
    jqTextGameOver: null,
    jqTextPaused: null,
    jqTextScore: null,
    jqPlayButton: null,
    jqMovGroundContainer: null,
    jqMovGroundFirst: null,
    movingGroundWidth: 0,
    MOVE_GROUND_PIXELS_PER_SECOND: 120,

    groundOffsetTopCheck: 0,
    Score: 0,


    HandleKeyInput: function (key) {
        if (this.State == GameStates.PreRunning) {
            if (key == Keys.Space) {
                Bird.Flap();
            }
        }
        else if (this.State == GameStates.Running) {
            if (key == Keys.Space) {
                Bird.Flap();
            }
            else if (key == Keys.P) {
                this.Pause();
            }
        }
        else if (this.State == GameStates.Paused) {
            if (key == Keys.P) {
                this.Resume();
            }
        }
        else if (this.State == GameStates.Ended) {
            if (key == Keys.Space) {
                this.Start();
            }
        }

    },
    Init: function() {
        Bird.InitVars();
        Pipes.InitVars();
        this.jqPlayButton = $(".play-button");
        this.jqTextGameOver = $(".text-gameover");
        this.jqTextPaused = $(".text-paused");
        this.jqTextScore = $(".text-score");
        this.jqMovGroundContainer = $(".movingGround-container");
        this.jqMovGroundFirst = $(".movingGround").first();
        this.movingGroundWidth = this.jqMovGroundFirst.width();

        this.groundOffsetTopCheck = Pipes.BG_HEIGHT - Bird.size.height;

        this.jqPlayButton.mousedown(function () {
            Game.Start();
        });
    },
    Start: function ()  {
        // Reset bird position
        Bird.Init();
        Pipes.Init();
        this.Resume();
        this.Score = 0;
        this.SetScore();
        this.State = GameStates.PreRunning;
    },
    Pause: function () {
        this.State = GameStates.Paused;
        SetVisibility(this.jqTextPaused, true);
        Bird.FlapAnim(false);
    },
    Resume: function () {
        this.State = GameStates.Running;
        SetVisibility(this.jqPlayButton, false);
        SetVisibility(this.jqTextGameOver, false);
        SetVisibility(this.jqTextPaused, false);
        SetVisibility(this.jqTextScore, true);
        

        Bird.FlapAnim(true);

        requestAnimationFrame(Run);

    },
    Ended: function () {
        this.State = GameStates.Ended;
        SetVisibility(this.jqPlayButton, true);
        SetVisibility(this.jqTextGameOver, true);
        Bird.FlapAnim(false);
    },



    Update: function (dt) {
        this.UpdateMovingGround(dt);
        if (this.State == GameStates.PreRunning) { return; }

        Bird.Update(dt);
        Pipes.Update(dt);
        this.UpdateScore();
        if (this.CheckGameOver()) {
            // Game Ended
            this.Ended();
            Bird.DeathAnimation.Start(dt);
        }
    },
    CheckGameOver: function () {
        if (this.CheckFloorContact()) { return true; }
        if (this.CheckPipeContact()) { return true; }
        return false;
    },
    CheckFloorContact: function () {
        return Bird.pos.top >= this.groundOffsetTopCheck;
    },
    CheckPipeContact: function () {
        if (Intersects(Bird.col, Pipes.pipes.Left.Top.col)) { return true; }
        if (Intersects(Bird.col, Pipes.pipes.Left.Bottom.col)) { return true; }
        if (Intersects(Bird.col, Pipes.pipes.Right.Top.col)) { return true; }
        if (Intersects(Bird.col, Pipes.pipes.Right.Bottom.col)) { return true; }
        return false;
    },
    UpdateMovingGround: function (dt) {
        var x = parseInt(this.jqMovGroundFirst.css("margin-left"));
        x -= this.MOVE_GROUND_PIXELS_PER_SECOND * dt;

        if ((x + this.movingGroundWidth) < 0) {
            // This div isn't visible anymore
            this.jqMovGroundFirst.remove();
            this.jqMovGroundFirst = $(".movingGround").first();
            this.jqMovGroundFirst.after('<div class="movingGround"></div>');

            // Recalculate x value
            var x = parseInt(this.jqMovGroundFirst.css("margin-left"));
            x -= this.MOVE_GROUND_PIXELS_PER_SECOND * dt;
        }
        this.jqMovGroundFirst.css("margin-left", x + "px");
    },
    UpdateScore: function () {
        if (Bird.pos.left > Pipes.CheckForScoringX) {
            this.Score += 1;
            this.SetScore();
            Pipes.CheckForScoringX = Pipes.pipes.Right.Top.pos.left + (Pipes.SIZE.width / 2);
        }
    },
    SetScore: function () {
        //this.jqTextScore.text(this.Score);
        this.jqTextScore.empty();

        var str = this.Score.toString();
        for (var i = 0, len = str.length; i < len; i++) {
            //alert(str[i]);

            $('<span class="number number' + str[i] + '"></span>').appendTo(this.jqTextScore);

        }


    }
}


var Pipes = {
    HOLE_HEIGHT: 100,
    PIPE_DISTANCE: 0,
    MIN_HOLE_TOP: 150,
    MAX_HOLE_TOP: 400,
    NEW_LEFT: 0,
    BG_HEIGHT: 0,
    SIZE: { width: 52, height: 320 },
    COLLISION_TOLERANCE: { x: 2, y: 1 },
    SPAWN_WHEN_LEFT_IS: -52,
    MOVE_PIXELS_PER_SECOND: 100,
    CheckForScoringX: 0,

    pipes: {
        Left: {
            Top:    { jq: null, pos: { left: 0, top: 0 }, col: { min: { x: 0, y: 0 }, max: { x: 0, y: 0 } } },
            Bottom: { jq: null, pos: { left: 0, top: 0 }, col: { min: { x: 0, y: 0 }, max: { x: 0, y: 0 } } },
        },
        Right: {
            Top: { jq: null, pos: { left: 0, top: 0 }, col: { min: { x: 0, y: 0 }, max: { x: 0, y: 0 } } },
            Bottom: { jq: null, pos: { left: 0, top: 0 }, col: { min: { x: 0, y: 0 }, max: { x: 0, y: 0 } } },
        },
        New: {
            Top: { jq: null, pos: { left: 0, top: 0 }, col: { min: { x: 0, y: 0 }, max: { x: 0, y: 0 } } },
            Bottom: { jq: null, pos: { left: 0, top: 0 }, col: { min: { x: 0, y: 0 }, max: { x: 0, y: 0 } } },
        },
    },



    InitVars: function () {
        var bgWidth = $(".background").width();
        this.PIPE_DISTANCE = bgWidth - ((bgWidth / 2) - (this.SIZE.width / 2));
        this.NEW_LEFT = this.PIPE_DISTANCE * 2 + this.SIZE.width*2;
        this.BG_HEIGHT = $(".background").height();

        this.MIN_HOLE_TOP = this.BG_HEIGHT - this.SIZE.height - (this.HOLE_HEIGHT / 2);
        this.MAX_HOLE_TOP = this.SIZE.height + (this.HOLE_HEIGHT / 2);
        

        this.pipes.Left.Top.jq = $('<div class="pipe pipe-top"></div>').appendTo(".background");
        this.pipes.Left.Bottom.jq = $('<div class="pipe pipe-bottom"></div>').appendTo(".background");
        this.pipes.Right.Top.jq = $('<div class="pipe pipe-top"></div>').appendTo(".background");
        this.pipes.Right.Bottom.jq = $('<div class="pipe pipe-bottom"></div>').appendTo(".background");
        this.pipes.New.Top.jq = $('<div class="pipe pipe-top"></div>').appendTo(".background");
        this.pipes.New.Bottom.jq = $('<div class="pipe pipe-bottom"></div>').appendTo(".background");
    },
    Init: function () {
        var bgwidth = $(".background").width();
        var offset = -100;
        var leftLeft = 0;
        var leftRight = this.PIPE_DISTANCE;
        var leftNew = this.PIPE_DISTANCE*2;
        

        this.SetUpNewPipe(this.pipes.Left);
        this.SetUpNewPipe(this.pipes.Right);
        this.SetUpNewPipe(this.pipes.New);
        this.MovePipes(this.pipes.Left, -leftLeft);
        this.MovePipes(this.pipes.Right, -leftRight);
        this.MovePipes(this.pipes.New, -leftNew);

        this.CheckForScoringX = this.pipes.Left.Top.pos.left + this.SIZE.width / 2;
}   ,
    Update: function (dt) {
        if (this.pipes.Left.Top.pos.left <= this.SPAWN_WHEN_LEFT_IS) {
            this.Spawn();
        }
        this.Move(this.MOVE_PIXELS_PER_SECOND * dt);
    },
    Move: function (movePixels) {
        this.MovePipes(this.pipes.Left, movePixels);
        this.MovePipes(this.pipes.Right, movePixels);
        this.MovePipes(this.pipes.New, movePixels);
        this.CheckForScoringX -= movePixels;
    },
    MovePipes: function (pipe, leftPixelsDelta) {
        pipe.Top.pos.left -= leftPixelsDelta;
        pipe.Bottom.pos.left -= leftPixelsDelta;
        SetPosition(pipe.Top.jq, pipe.Top.pos);
        SetPosition(pipe.Bottom.jq, pipe.Bottom.pos);

        // Also move collision infos
        pipe.Top.col.min.x -= leftPixelsDelta;
        pipe.Top.col.max.x -= leftPixelsDelta;
        pipe.Bottom.col.min.x -= leftPixelsDelta;
        pipe.Bottom.col.max.x -= leftPixelsDelta;
    },
    Spawn: function () {
        var newPipes = this.pipes.Left;
        this.pipes.Left = this.pipes.Right;
        this.pipes.Right = this.pipes.New;

        this.SetUpNewPipe(newPipes);
        this.pipes.New = newPipes;
    },
    SetUpNewPipe: function (pipe) {
        var newHole = RandomInt(this.MIN_HOLE_TOP, this.MAX_HOLE_TOP);
        pipe.Top.pos.left = this.NEW_LEFT;
        pipe.Top.pos.top = (newHole - (this.HOLE_HEIGHT / 2)) - this.SIZE.height;
        pipe.Bottom.pos.left = this.NEW_LEFT;
        pipe.Bottom.pos.top = newHole + (this.HOLE_HEIGHT / 2);
        
        SetPosition(pipe.Top.jq, pipe.Top.pos);
        SetPosition(pipe.Bottom.jq, pipe.Bottom.pos);

        // Setup collision info
        pipe.Top.col.min.x = pipe.Top.pos.left + this.COLLISION_TOLERANCE.x;
        pipe.Top.col.min.y = pipe.Top.pos.top + this.COLLISION_TOLERANCE.y;
        pipe.Top.col.max.x = (pipe.Top.pos.left + this.SIZE.width) - this.COLLISION_TOLERANCE.x;
        pipe.Top.col.max.y = (pipe.Top.pos.top + this.SIZE.height) - this.COLLISION_TOLERANCE.y;

        pipe.Bottom.col.min.x = pipe.Bottom.pos.left + this.COLLISION_TOLERANCE.x;
        pipe.Bottom.col.min.y = pipe.Bottom.pos.top + this.COLLISION_TOLERANCE.y;
        pipe.Bottom.col.max.x = (pipe.Bottom.pos.left + this.SIZE.width) - this.COLLISION_TOLERANCE.x;
        pipe.Bottom.col.max.y = (pipe.Bottom.pos.top + this.SIZE.height) - this.COLLISION_TOLERANCE.y;
    }
}


var Bird = {
    jqdiv: null,
    startPos: null,
    pos: { left: 0, top: 0 },
    col: { min: { x:0, y:0 }, max: { x:0, y:0 }},
    size: null,
    FLAP_ROTATION: -30,
    FLAP_ROTATION_PER_SECOND: 200,
    MAX_ROTATION: 90,
    rotation: 0,

    MOVE_DOWN_PIXELS_PER_SECOND: 250,

    Flapping: {
        FLAP_TIME: 0.3,
        timerFlapping: 0,
        isFlapping: false,
        flapValue: 0,
        FLAP_UP_POWER: 13,
        Update: function (dt) {
            this.timerFlapping -= dt;
            this.isFlapping = this.timerFlapping > 0;
            if (this.isFlapping) {
                this.flapValue = this.FLAP_UP_POWER * (this.timerFlapping / this.FLAP_TIME);
            } else {
                this.flapValue = 0;
            }
        }
    },
    DeathAnimation: {
        MOVE_PIXELS_PER_SECOND: 600,
        isPlaying: true,


        Play: function (dt) {
            if (!this.isPlaying) { return; }

            Bird.pos.top += this.MOVE_PIXELS_PER_SECOND * dt;
            this.isPlaying = Bird.pos.top < Game.groundOffsetTopCheck;

            if (this.isPlaying) {
                SetPosition(Bird.jqdiv, Bird.pos);
                SetRotation(Bird.jqdiv, Bird.MAX_ROTATION);
                requestAnimationFrame(RunDeathAnim);
            }            
        },
        Start: function(dt) {
            this.isPlaying = true;
            requestAnimationFrame(RunDeathAnim);
        },
        Stop: function () {
            this.isPlaying = false;
        }
    },

    InitVars: function() {
        this.jqdiv = $("#bird");
        this.jqdiv.addClass("startposition");
        this.startPos = GetPosition(this.jqdiv);
        this.size = GetSize(this.jqdiv);
        this.jqdiv.removeClass("startposition");

        this.Init();
    },
    Init: function () {
        this.DeathAnimation.Stop();

        this.FlapAnim(true);
        this.Flapping.timerFlapping = this.Flapping.FLAP_TIME;
        this.Flapping.isFlapping = false;
        this.Flapping.flapValue = 0;
        this.pos.left = this.startPos.left;
        this.pos.top = this.startPos.top;
        this.col.min.x = this.pos.left;
        this.col.min.y = this.pos.top;
        this.col.max.x = this.pos.left + this.size.width;
        this.col.max.y = this.pos.top + this.size.height;
        this.rotation = 0;
        
        SetPosition(this.jqdiv, this.pos);
        SetRotation(this.jqdiv, this.rotation);
    },
    FlapAnim: function (doFlap) {
        if (doFlap == undefined) { doFlap = true; }
        this.jqdiv.toggleClass("flapping", doFlap);
    },
    Flap: function () {
        this.Flapping.timerFlapping = this.Flapping.FLAP_TIME;
        this.rotation = this.FLAP_ROTATION;
        SetRotation(this.jqdiv, this.rotation);

        Game.State = GameStates.Running;
    },
    Update: function (dt) {
        this.Flapping.Update(dt);
        
        // MOVE
        this.pos.top -= this.Flapping.flapValue; // Move up when flapping        
        this.pos.top += this.MOVE_DOWN_PIXELS_PER_SECOND * dt; // Move down
        SetPosition(this.jqdiv, this.pos);

        // Rotate
        if (!this.Flapping.isFlapping && this.rotation < this.MAX_ROTATION) {
            // Rotate downwards
            this.rotation += this.FLAP_ROTATION_PER_SECOND * dt;
            this.rotation = this.rotation > this.MAX_ROTATION ? this.MAX_ROTATION : this.rotation;
            SetRotation(this.jqdiv, this.rotation);
        }

        // Set colision info
        if (this.pos.top < 0) {
            // Prevent flying over all the pipes
            this.col.min.y = 0;
            this.col.max.y = this.size.height;
        } else {
            this.col.min.y = this.pos.top;
            this.col.max.y = this.pos.top + this.size.height;
        }
    }
}




$(document).ready(function () {
    $(document).keydown(function (event) {
        event.preventDefault();
        Game.HandleKeyInput(event.keyCode);
    });

    $(document).mousedown(function () {
        if (Game.State == GameStates.Running || Game.State == GameStates.PreRunning) {
            Game.HandleKeyInput(Keys.Space);
        }
    });

    // Initialize
    Game.Init();
    Game.Start();
});


function Run(timestamp) {
    if (Game.State == GameStates.Paused) { return; }
    if (Game.State == GameStates.Ended) { return; }

    // Game is running
    Time.Update(timestamp);
    Game.Update(Time.DT);
    requestAnimationFrame(Run);
}

function RunDeathAnim(timestamp) {
    Time.Update(timestamp);
    Bird.DeathAnimation.Play(Time.DT);
}


function SetVisibility(jqueryElement, isVisible) {
    if (isVisible == undefined) { isVisible = true; }
    jqueryElement.toggleClass("hidden", !isVisible);
}

function GetPosition(jqueryElement) {
    var pos = jqueryElement.position();
    return { left: pos.left, top: pos.top };
}

function SetPosition(jqueryElement, pos) {
    jqueryElement.css("left", pos.left + "px");
    jqueryElement.css("top", pos.top + "px");
}

function GetSize(jqueryElement) {
    return { width: jqueryElement.width(), height: jqueryElement.height() };    
}

function SetSize(jqueryElement, size) {
    jqueryElement.width(size.width);
    jqueryElement.height(size.height);
}


function Intersects(bird, pipe) {
    if (bird.max.x < pipe.min.x) { return false; }
    if (bird.min.x > pipe.max.x) { return false; }
    if (bird.max.y < pipe.min.y) { return false; }
    if (bird.min.y > pipe.max.y) { return false; }
    return true;
}

function SetRotation(jq, rotation) {
    var rotate = "rotate("+rotation+"deg)";

    jq.css("-webkit-transform", rotate);
    jq.css("-moz-transform", rotate);
    jq.css("-o-transform", rotate);
    jq.css("-ms-transform", rotate);
}

function RandomInt(a, b) { return a + Math.floor(Math.random() * (1 + b - a)); }
